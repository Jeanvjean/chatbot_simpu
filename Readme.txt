to test the applicateion, run the app.js fine using node app.js or install nodemon on your local envirument and simply run nodemon.
then get ngrok on your local device, and run ngrok http 3000 to create a tunel that renders the localhost to a domain on ngrok.io
create 2 twilio accounts, (because you can only get one free number per account,)

replace this as follows in the createdb.js file
const accountSid1 = 'Sid for first account';
const authToken1 = 'auth token for first account';

const accountSid2 = 'sid for second account ';
const authToken2 = 'auth token for second account';

in your twilio account1, navigate to manage number and then replace the message url at the bottom of the page, its a post request
with the url provided for you by ngrok it should look somthing like this https://4dbc83dc.ngrok.io/create_account.
the create_account refers to the route for incoming message in the route.js file (for account creation).
do the same for the second account except this time the route is transfer_fund.

now if a message is sent to the account one phone number, it would hit the create_account endpoint and if a message is sent to 
the account 2 phone number it would hit the transfer fund endpoint

=====messages ======
to create an account send CREATE to the twilio account 1 phone number
and follow the replied instruction.

to check ballance of account
-send Ballance to twilio account 2 phone number 
    you will get a reply with your account ballance

to transfer fund to another account
-send Transfer to twilio account2 phone number
 and follow the replied instruction

to create a mysql database 
-use localhost:3000/createdb
- or use the ngrok url and add the route eg /createdb

to create tables 
- use localhost:3000/create_tables
- or use the ngrok url and add the route eg /create_tables

NB. you would need to have node installed on your machine 

then run npm install to get all the dependencies used
