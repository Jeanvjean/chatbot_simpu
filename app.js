const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');


require('./app/migrations/dbconnect/connect');

var database = require('./app/migrations/route');

app.use(cors());
app.use(morgan('dev'));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))


app.use(database);

app.listen(port,()=>{
    console.log(`app is running on port: ${port}`)
});