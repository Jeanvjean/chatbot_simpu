const db = require('./dbconnect/connect');

const twilio = require('twilio');

const accountSid1 = 'AC36a4ee2e19afa27b406e22da50f4a955';
const authToken1 = '86c1bf5e23e70425bce3918a0acbc563';

const accountSid2 = 'AC88d631400f934f2232520dc616a844e9';
const authToken2 = '065229a214e6e230dd3f59655020ad7e';

const client = new twilio(accountSid1,authToken1);
const fund_transfer = new twilio(accountSid2,authToken2);

var tables = [
    'CREATE TABLE accounts(id int AUTO_INCREMENT, phone_number VARCHAR(256), name VARCHAR(255), account_number int, ballance VARCHAR(256), PRIMARY KEY(id) )',
    'CREATE TABLE transfers(id int AUTO_INCREMENT, from_num VARCHAR(256), to_acc VARCHAR(256),amount VARCHAR(256), PRIMARY KEY(id))'
];

module.exports  = {
    create:async(req,res)=>{
        let sql = 'CREATE DATABASE chatbot';
        db.query(sql,(err,result)=>{
            if (err) {
                throw err;
            }
            console.log(result);
            res.send('Database created')
        })
    },
    create_table:async(req,res)=>{
        for (let i = 0; i < tables.length; i++) {
            const element = tables[i];
            db.query(element,(err,result)=>{
                if (err) {
                    throw err;
                }
                console.log(result);
                res.send('Table Added');
            })
            
        }
    },
    message:async(req,res)=>{
        let from  = req.body.From;
        let to = req.body.To;
        let body = req.body.Body;
        
        var account = db.query("SELECT * FROM accounts where phone_number = $1",
            [from]
        );
        // console.log(account.rows);
        if (account.length !== 0) {
            //do somthing i.e continue conversation
            if (!account.name && !account.ballance) {
                db.query("UPDATE accounts set name = $1",
                [body])
                client.messages.create({
                    to: `${from}`,
                    from: `${to}`,
                    body: 'How much would you like to open with?'
                });
                res.end();
            }else if(!account.ballance){
                var ac_num = Math.floor((Math.random() * 100000000000) + 1);
                db.query("UPDATE accounts set ballance = $1, account_number = $2",
                [body,ac_num])
                client.messages.create({
                    to: `${from}`,
                    from: `${to}`,
                    body: `Thank you for choosing us here is your account number ${ac_num}...`
                });
                res.end();
            }
        } else {
            if (body == 'CREATE') {
                db.query("INSERT INTO accounts(phone_number), VALUES($1) ",[
                    from
                ])
                clientInformation.messages.create({
                    to: `${from}`,
                    from: `${to}`,
                    body: 'What is your fullname?'
                })
                res.end();
            }
        }
        res.end();
    },
    transfer:async(req,res)=>{
        let from  = req.body.From;
        let to = req.body.To;
        let body = req.body.Body;

        var account = db.query("SELECT * FROM accounts where phone_number = $1",
            [from]
        )
        if (account.rows !==0) {
            
            if (body == 'Transfer') {
                var existing = db.query("SELECT * FROM transfers where from_num = $1",
                    [from]
                );
                if (existing.length !== 0) {
                    db.query("UPDATE transfers set from_num = $1, amount = $2, to_acc = $3",
                        [from,null,null]
                    );
                    fund_transfer.messages.create({
                        from: `${to}`,
                        to: `${from}`,
                        body: 'enter recipient account number'
                    });
                    res.end();
                }else{
                    db.query("INSERT INTO transfers(from_num), VALUES($1)",
                        [from]
                    );
                    fund_transfer.messages.create({
                        from: `${to}`,
                        to: `${from}`,
                        body: 'enter recipient account number'
                    })
                    res.end();
                }
            } else if(body == "Ballance") {
                var account = db.query("SELECT * FROM accunts where phone_number = $1",
                    [from]
                );
                fund_transfer.messages.create({
                    body: `Your account ballance is ${account.ballance}`,
                    to: `${from}`,
                    from: `${to}`
                });
                res.end();
            }else{
                var existing = db.query("SELECT * FROM transfers where from_num = $1",
                    [from]
                );
                if (!existing.amount && !existing.to_acc) {
                    db.query("UPDATE transfers set to_acc = $1",
                        [body]
                    );
                    fund_transfer.messages.create({
                        from: `${to}`,
                        to: `${from}`,
                        body: 'enter amount to transfer'
                    });
                    res.end();
                }else if(!existing.amount){
                    db.query("UPDATE transfers set amount = $1",
                        [body]
                    );
                    fund_transfer.messages.create({
                        from:`${to}`,
                        to: `${from}`,
                        body: 'Fund transfer was successful. thank you for banking with us.'
                    });
                    res.end();
                }
            }
        }else{
            fund_transfer.messages.create({
                from: `${to}`,
                to: `${from}`,
                body: 'Sorry you do not have an account with us. Text CREATE to +1 203 457 8285 to create an account.'
            })
        }
    }
}