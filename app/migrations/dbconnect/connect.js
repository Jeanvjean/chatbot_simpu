
const mysql = require('mysql');

//cxreate connection
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'chatbot'
});

//connect
db.connect((err)=>{
    if(err){
        console.log(err);
    }
    console.log('Connected to MysqlDB');
})

module.exports = db;