const express = require('express');
const router = express.Router();

var dbController = require('./createbd');

router.route('/createdb')
    .get(dbController.create);

router.route('/create_tables')
    .get(dbController.create_table);

router .route('/create_account')
    .post(dbController.message);

router.route('/transfer_fund')
    .post(dbController.transfer);

    module.exports = router;